import re


class TextToHtml:
    text: str = ''
    url_pattern: str = r'(?:https?:\/\/|ftps?:\/\/|www\.)(?:(?![.,?!;:()]*(?:\s|$|”))[^\s”]){2,}'
    email_pattern: str = r'[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+'
    paragraphs_end_with_dash_and_equation_pattern: str = r'\n=+</p>|\n-+</p>'
    new_r_pattern: str = r'\r'
    new_line_pattern: str = r'\n'
    empty_line_pattern: str = r'\n\n'
    tag_a_url: str = '<a href="{0}">{0}</a>'
    tag_a_email: str = '<a href="mailto:{0}">{0}</a>'
    tag_p: str = '<p>{0}</p>\n'
    tag_h1: str = '<h1>{0}</h1>\n'
    tag_hn: str = '<h{1}>{0}</h{1}>\n'
    tag_ul: str = '<ul>{0}</ul>\n'
    tag_li: str = '<li>{0}</li>\n'

    start_asterisk: str = '* '
    start_sign: str = '#'
    many_symbol: str = '+'

    def __init__(self, input_text: str):
        self.text = input_text

    def convert(self):
        """
        Converts plain text to HTML.

        Converter implemented the following rules:
            1.  Convert all URLs and emails into valid HTML links.
            2.  Convert all text lines followed one by one and surrounded with
                empty lines into an HTML paragraph.
            3.  Convert all paragraphs that end with a line filled by - (dash)
                or = (equation) into HTML header of first level.
            4.  Convert all paragraphs that start from two or more number signs
                into HTML headers of 2nd and other levels, depending on number of
                number signs.
            5.  Convert all paragraphs that start from asterisk into unordered
                list item. A few such paragraphs must be converted into a single
                unordered list with several list items.
        """

        self.text = re.sub(f'{self.new_r_pattern}', '', self.text)
        self.handle_links()
        self.handler_paragraphs()

        return self.text

    def handle_links(self):
        """
        Convert all URLs and emails into valid HTML links.

        Examples:
            www.yandex.com - <a href="www.yandex.com">www.yandex.com</a>
            gmail@gmail.com - <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
        """
        self.text = self._replace_links_to_html_tag_a(self.url_pattern)
        self.text = self._replace_links_to_html_tag_a(self.email_pattern)

    def _replace_links_to_html_tag_a(self, pattern: str) -> str:
        """
        Replace links to html tag <a href=""></a>.

        :param pattern: str
        :return: str
        """
        tmp_text: str = self.text
        for link in re.findall(pattern, tmp_text):
            if self.email_pattern == pattern:
                tag_a_email = self.tag_a_email
                tmp_text = re.sub(link, tag_a_email.format(link), tmp_text)
            else:
                tag_a_url = self.tag_a_url
                tmp_text = re.sub(link, tag_a_url.format(link), tmp_text)
        return tmp_text

    def handler_paragraphs(self):
        """
        Convert all paragraphs that end with a line filled by - (dash)
        or = (equation) into HTML header of first level

        Examples:
            Devil’s Hands are Idle Playthings
            =================================
            to
            <h1>Devil’s Hands are Idle Playthings</h1>

            Time Keeps on Slippin’
            ----------------------
            <h1>Time Keeps on Slippin’</h1>
        """
        self.text = self._replace_paragraphs_to_html()
        self.text = self._replace_paragraphs_end_with_dash_and_equation_to_html_tag_h1()
        self.handle_paragraphs_end_with_dash_and_equation()

    def _replace_paragraphs_to_html(self) -> str:
        """
        Replace paragraphs to html tag <p></p>

        :return: str
        """
        tmp_text: str = ''
        tag_p = self.tag_p
        for words in re.split(self.empty_line_pattern, self.text):
            paragraph = tag_p.format(words)
            paragraph = self.handle_paragraphs_begin_asterisk(paragraph)
            paragraph = self.handle_paragraphs_begin_sign(paragraph)
            tmp_text += paragraph

        return tmp_text

    def handle_paragraphs_begin_asterisk(self, paragraph: str) -> str:
        """
        Convert asterisk to unordered list item

        Examples:
            *   Alright, let’s mafia things up a bit. Joey, burn down the ship. Clamps, burn down the crew.
            *   Meh.
            *   We need rest. The spirit is willing, but the flesh is spongy and bruised.
            *   No, of course not. It was… uh… porno. Yeah, that’s it.
            to
            <ul>
                <li>Alright, let’s mafia things up a bit. Joey, burn down the ship. Clamps, burn down the crew.</li>
                <li>Meh.</li>
                <li>We need rest. The spirit is willing, but the flesh is spongy and bruised.</li>
                <li>No, of course not. It was… uh… porno. Yeah, that’s it.</li>
            </ul>

        :param paragraph: str
        :return: str
        """
        if self.tag_p[:3] + self.start_asterisk in paragraph:
            paragraph = self._replace_paragraphs_begin_asterisk_to_html_unordered_list(paragraph)

        return paragraph

    def _replace_paragraphs_begin_asterisk_to_html_unordered_list(self, paragraph: str) -> str:
        """
        Replace asterisk to html tag <ul><li></li></ul>

        :param paragraph: str
        :return: str
        """
        tag_ul = self.tag_ul
        begin_tag_ul = tag_ul[:4]
        tag_li = self.tag_li
        for asterisk in re.split(self.new_line_pattern, paragraph):
            if self.tag_p[:3] + self.start_asterisk in asterisk:
                li = tag_li.format(asterisk[5:])
                begin_tag_ul += li
            elif self.tag_p[1:3] in asterisk:
                li = tag_li.format(asterisk[2:-4])
                begin_tag_ul += li
            elif self.start_asterisk in asterisk and self.tag_p[-5:-1] not in asterisk and self.tag_p[:3] in asterisk:
                li = tag_li.format(asterisk[2:])
                begin_tag_ul += li
        begin_tag_ul += tag_ul[-6:]
        return begin_tag_ul

    def handle_paragraphs_begin_sign(self, paragraph: str) -> str:
        """
        Convert signs to header of

        Examples:
            ## Jurassic Bark
            to
            <h2>Jurassic Bark</h2>

            ## Space Pilot 3000
            to
            <h2>Space Pilot 3000</h2>

        :param paragraph: str
        :return: str
        """
        if signs := re.findall(self.tag_p[0:3] + self.start_sign + self.many_symbol, paragraph):
            paragraph = self._replace_paragraphs_begin_sign_to_html_header(signs[0], paragraph)

        return paragraph

    def _replace_paragraphs_begin_sign_to_html_header(self, signs: str, paragraph) -> str:
        """
        Replace signs to html tag <hn></hn>, where n - number of signs

        :param signs: str
        :param paragraph: str
        :return: str
        """
        sign_cnt = signs.count(self.start_sign)
        hn = re.split(self.tag_p[-5:-1], re.split(self.tag_p[0:3] + self.start_sign + self.many_symbol, paragraph)[1])
        tag_hn = self.tag_hn
        return tag_hn.format(hn[0], sign_cnt)

    def handle_paragraphs_end_with_dash_and_equation(self):
        """
        Convert all paragraphs that end with a line filled by - (dash)
        or = (equation) into HTML header of first level

        Examples:
            Devil’s Hands are Idle Playthings
            =================================
            to
            <h1>Devil’s Hands are Idle Playthings</h1>

            Time Keeps on Slippin’
            ----------------------
            <h1>Time Keeps on Slippin’</h1>
        """
        self.text = self._replace_paragraphs_end_with_dash_and_equation_to_html_tag_h1()

    def _replace_paragraphs_end_with_dash_and_equation_to_html_tag_h1(self) -> str:
        """
        Replace paragraphs that end with a line filled by - (dash)
        or = (equation) to html tag <h1></h1>.
        :return: str
        """
        tmp_text: str = ''
        tag_h1 = self.tag_h1
        for paragraph in re.split(self.paragraphs_end_with_dash_and_equation_pattern, self.text):
            if self.tag_p[:3] in paragraph and self.tag_p[-4:] not in paragraph:
                tmp_text += tag_h1.format(paragraph[3:])
            else:
                tmp_text += paragraph
        return tmp_text
