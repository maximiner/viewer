import os
import re


class ReaderFileSystem:
    supported: tuple = ('.html', '.txt')

    def __init__(self, input_path: str = 'apps/viewer/fixtures'):
        self.path = input_path

    def upload_dir(self) -> list[tuple[str, str, str]]:
        files: list[str] = self.get_files()
        pages: list[tuple[str, str, str]] = list()
        for file in files:
            pages.append(self.read_file(file))

        return pages

    def get_files(self) -> list[str]:
        files: list[str] = [
            os.path.join(p, name) for (p, d, f) in os.walk(self.path) for name in f if
            name.endswith(self.supported[0]) or name.endswith(self.supported[1])
        ]
        return files

    def read_file(self, file_name: str) -> tuple[str, str, str]:
        is_head: bool = True
        file: str = ''
        title: str = ''
        with open(file_name, 'r') as f:
            for line in f.readlines():
                if is_head:
                    title = line
                    file += line
                    is_head = False

                file += line

            if f.name.endswith(self.supported[0]):
                mime = 'text/html'
                if '<head>' in file or '<title>' in file:
                    title = file[file.find('<title>') + 7:file.find('</title>')]
                    title = title.strip()
                elif '<p><b>' in file and '<span style="font-size: 11px; line-height: 0px;">&nbsp;</span>' in file:
                    span_pattern = '<span style="font-size: 11px; line-height: 0px;">&nbsp;</span>'
                    title = file[file.find('<p><b>') + 6:file.find(span_pattern)]
                    title = title.strip()
                else:
                    title = re.sub(r'<[a-z0-9]+>|</[a-z0-9]+>', '', title)
                    title = title.strip()
            elif f.name.endswith(self.supported[1]):
                mime = 'text/plain'
                title = title.strip()
            else:
                mime = ''

        return mime, title, file
