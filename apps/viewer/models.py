from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.utils.models.common import TimeStampModelMixin


class Page(TimeStampModelMixin):
    mime = models.CharField(
        verbose_name=_('mime'),
        max_length=255
    )

    title = models.CharField(
        verbose_name=_('title'),
        max_length=255
    )

    text = models.TextField(
        verbose_name=_('text'),
    )

    class Meta:
        """"""
        db_table = 'page'

        verbose_name = _('Page')
        verbose_name_plural = _('Pages')

    def __str__(self) -> str:
        """"""
        return f"{self.pk}-{self.title}"


class Link(TimeStampModelMixin):
    link = models.CharField(
        verbose_name=_('link'),
        max_length=255
    )

    page = models.ForeignKey(
        Page,
        on_delete=models.CASCADE,
        related_name='link',
        verbose_name=_('page')
    )

    class Meta:
        """"""
        db_table = 'link'

        verbose_name = _('Link')
        verbose_name_plural = _('Links')

    def __str__(self) -> str:
        """"""
        return f"{self.pk}-{self.link}"

    @classmethod
    def create_link_with_page(cls, mime, title, text, link):
        page = Page.objects.create(mime=mime, title=title, text=text)
        cls.objects.create(link=link, page=page)
