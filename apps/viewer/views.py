import asyncio

from django.db.models import Q
from django.http import HttpResponse
from django.template import loader
from django.views import View
from django.utils import timezone

from apps.viewer.models import Page, Link
from apps.utils.common.text_to_html import TextToHtml


class PageView(View):
    async def get(self, request, *args, **kwargs):
        start_t = timezone.now()
        query = request.GET.get("q")
        links = list()
        if query:
            links = await Page.objects.filter(
                Q(title__icontains=query)).afirst()
        else:
            async for page in Page.objects.filter():
                links.append(await page.link.afirst())

        template = loader.get_template('viewer/child.html')
        text = None
        title = "Viewer"
        if links and query:
            text = links.text
            title = links.title
            import pprint
            pprint.pprint(text)
            if links.mime == 'text/plain':
                txt_to_html = TextToHtml(links.text)
                text = txt_to_html.convert()

        end_t = timezone.now()
        context = {
            'text': text,
            'title': title,
            'links': links,
            'time': (end_t - start_t).microseconds
        }
        # await asyncio.sleep(3)
        return HttpResponse(template.render(context, request))
