import sys

from django.apps import AppConfig


class ViewerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.viewer'

    def ready(self):

        if 'runserver' not in sys.argv:
            print("server down")
        else:
            from apps.utils.common.reader import ReaderFileSystem
            from .models import Link, Page
            r = ReaderFileSystem(input_path='apps/viewer/fixtures/')
            files = r.upload_dir()
            files_name = [f[1] for f in files]
            pages = Page.objects.filter(title__in=files_name).values_list('title', flat=True)
            for file in files:
                if file[1] not in pages:
                    Link.create_link_with_page(file[0], file[1], file[2], file[1])
