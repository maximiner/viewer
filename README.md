## Start up
* ```docker-compose -f docker-compose.yml up --build```
* ```python manage.py runserver```

## Create new DB

   - Delete `db.sqlite3`
   - Execute `python manage.py migrate`

### Description
* go to http://localhost:8000 (main page)
* on main page have list of pages, form for search page of list and timer
* add new page to list with **fixtures** directory in `apps/viewer/` and restart server


